#!/bin/sh
CAN_I_RUN_SUDO=$(sudo -n uptime 2>&1|grep "load"|wc -l)
 if [ ${CAN_I_RUN_SUDO} -gt 0 ]
 then
	 echo "Sudo test succeded!"
 else
	 echo "You do not have sudo installed, or you aren't using it."
     exit
 fi
 echo "Welcome to the ShInstall Install System!"
 echo "ShInstall version 18.04 (beta 0.0.1) from GalactiDev"
#Change the part labeled PROGRAM NAME HERE! with the name of your program and the last part of this line to your download link for the file.
 if (whiptail --title "Prepairing to Install" --yesno "Prepairing to install the program Python from the files on https://github.com/python/cpython using Make: do you wish to continue?" 12 78) then 
     #insert your install code below
    wget https://www.python.org/ftp/python/3.6.5/Python-3.6.5.tar.xz
    tar -xvf Python-3.6.5.tar.xz
    cd Python-3.6.5
    ./configure
    make
    make test
    make install
    #Include this code as well
    whiptail --title "Installation Complete!" --infobox "The installation was successful!" 10 60
else
    whiptail --title "Installation canceled by user" --infobox "The installation was canceled by the user. Now exiting" 10 60
    exit
fi